<?php
/**
 * Created by PhpStorm.
 * User: viniciushrk
 * Date: 05/10/2018
 * Time: 15:05
 */

require_once "Conexao.php";

class Produto extends Conexao
{
    private $id_produto;
    private $lote;
    private $descricao;
    private $data_aquisicao;
    private $hora_aquisicao;
    private $Fornecedor_id_Fornecedor;

    public function seleciona($idCurso)
    {
        try {
            $con = $this->conecta();
            $resul = $con->prepare("select * from curso where idCurso = ?");
            $resul->bindValue(1, $idCurso);
            $resul->execute();
            $con = null;
            if ($resul->rowCount() > 0) {
                $resul = $resul->fetch();
                $this->idCurso = $resul[0];
                $this->nome_curso = $resul[1];
            } else {
                return 0;
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }


}