<?php
/**
 * Created by PhpStorm.
 * User: viniciushrk
 * Date: 05/10/2018
 * Time: 14:53
 */
require_once "Conexao.php";
require_once "php-mysql-aes-crypt-master/src/Crypter.php";
class Fornecedor extends Conexao
{

    private $id_fornecedor;
    private $razao_social;
    private $nome_fantasia;
    private $cnpj;
    private $cep;
    private $municipio;
    private $bairro;
    private $rua;
    private $numero;
    private $uf;
    private $ddd;
    private $telefone;
    private $Empresa_id_empresa;




    public function seleciona($id_fornecedor)
    {
        try {
            $con = $this->conecta();
            $resul = $con->prepare("select * from servidor where id_fornecedor = ?");
            $resul->bindValue(1,  $id_fornecedor);
            $resul->execute();
            $con = null;
            if ($resul->rowCount() > 0) {
                $resul = $resul->fetch();
                $this->id_fornecedor = $resul[0];
                $this->razao_social = $resul[1];
                $this->nome_fantasia = $resul[2]$
                $this->cnpj = $resul[3];
                $this->cep =$resul[4];
                $this->municipio = $resul[5];
                $this->bairro = $resul[6];
                $this->rua = $resul[7];
                $this->numero = $resul[8];
                $this->uf = $resul[9];
                $this->ddd = $resul[10];
                $this->telefone = $resul[11];
                $this->Empresa_id_empresa = $resul[12];


            } else {
                return 0;
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getRazaoSocial()
    {
        return $this->razao_social;
    }

    /**
     * @param mixed $razao_social
     */
    public function setRazaoSocial($razao_social)
    {
        $this->razao_social = $razao_social;
    }

    /**
     * @return mixed
     */
    public function getNomeFantasia()
    {
        return $this->nome_fantasia;
    }

    /**
     * @param mixed $nome_fantasia
     */
    public function setNomeFantasia($nome_fantasia)
    {
        $this->nome_fantasia = $nome_fantasia;
    }

    /**
     * @return mixed
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param mixed $cnpj
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * @param mixed $municipio
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getRua()
    {
        return $this->rua;
    }

    /**
     * @param mixed $rua
     */
    public function setRua($rua)
    {
        $this->rua = $rua;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }

    /**
     * @return mixed
     */
    public function getDdd()
    {
        return $this->ddd;
    }

    /**
     * @param mixed $ddd
     */
    public function setDdd($ddd)
    {
        $this->ddd = $ddd;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return mixed
     */
    public function getEmpresaIdEmpresa()
    {
        return $this->Empresa_id_empresa;
    }

    /**
     * @param mixed $Empresa_id_empresa
     */
    public function setEmpresaIdEmpresa($Empresa_id_empresa)
    {
        $this->Empresa_id_empresa = $Empresa_id_empresa;
    }





}