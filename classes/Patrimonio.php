<?php
/**
 * Created by PhpStorm.
 * User: viniciushrk
 * Date: 05/10/2018
 * Time: 16:08
 */

require_once "Conexao.php";

class Patrimonio extends Conexao
{
    private $tombo;
    private $data_registro;
    private $descricao;
    private $status;
    private $Tipo_patrimonio_id_tipo;


    public function seleciona($idFaltas)
    {
        try {
            $con = $this->conecta();
            $resul = $con->prepare("select * from faltas where idFaltas = ?");
            $resul->bindValue(1, $idFaltas);
            $resul->execute();
            $con = null;
            if ($resul->rowCount() > 0) {
                $resul = $resul->fetch();
                $this->idFaltas = $resul[0];
                $this->data_inicio = $resul[1];
                $this->aluno_num_matricula = $resul[2];
                $this->motivo_idMotivo= $resul[3];
            } else {
                return 0;
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }



}