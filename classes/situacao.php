<?php
/**
 * Created by PhpStorm.
 * User: viniciushrk
 * Date: 05/10/2018
 * Time: 15:14
 */

class Situcao extends Conexao
{
    private $id_situacao;
    private $situacao;


    public function seleciona($id_situacao)
    {
        try {
            $con = $this->conecta();
            $resul = $con->prepare("select * from turma where id_situacao = ?");
            $resul->bindValue(1, $id_situacao);
            $resul->execute();
            $con = null;
            if ($resul->rowCount() > 0) {
                $resul = $resul->fetch();
                $this->id_situacao = $resul[0];
                $this->situacao = $resul[1];
                return false;
            } else {
                return true;
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param mixed $situacao
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    }


}