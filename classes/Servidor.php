<?php
/**
 * Created by PhpStorm.
 * User: viniciushrk
 * Date: 05/10/2018
 * Time: 15:14
 */

class Servidor extends Conexao
{
    private $matricula;
    private $nome;
    private $sexo;
    private $data_nasc;
    private $cep;
    private $municipio;
    private $bairro;
    private $rua;
    private $numero;
    private $uf;
    private $ddd;
    private $telefone;
    private $Cargo_id_cargo;
    private $email;
    private $senha;
    private $Situacao_id_situacao;
    private $Setor_id_setor;


    public function seleciona($matricula)
    {
        try {
            $con = $this->conecta();
            $resul = $con->prepare("select * from turma where matricula = ?");
            $resul->bindValue(1, $matricula);
            $resul->execute();
            $con = null;
            if ($resul->rowCount() > 0) {
                $resul = $resul->fetch();
                $this->matricula = $resul[0];
                $this->nome = $resul[1];
                $this->sexo= $resul[2];
                $this->data_nasc= $resul[3];
                $this->cep= $resul[4];
                $this->municipio= $resul[5];
                $this->bairro= $resul[6];
                $this->numero= $resul[7];
                $this->uf= $resul[8];
                $this->ddd= $resul[9];
                $this->telefone= $resul[10];
                $this->Cargo_id_cargo= $resul[11];
                $this->email= $resul[12];
                $this->senha= $resul[13];
                $this->Situacao_id_situacao= $resul[14];
                $this->Setor_id_setor= $resul[15];




                return false;
            } else {
                return true;
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getDataNasc()
    {
        return $this->data_nasc;
    }

    /**
     * @param mixed $data_nasc
     */
    public function setDataNasc($data_nasc)
    {
        $this->data_nasc = $data_nasc;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * @param mixed $municipio
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getRua()
    {
        return $this->rua;
    }

    /**
     * @param mixed $rua
     */
    public function setRua($rua)
    {
        $this->rua = $rua;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }

    /**
     * @return mixed
     */
    public function getDdd()
    {
        return $this->ddd;
    }

    /**
     * @param mixed $ddd
     */
    public function setDdd($ddd)
    {
        $this->ddd = $ddd;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return mixed
     */
    public function getCargoIdCargo()
    {
        return $this->Cargo_id_cargo;
    }

    /**
     * @param mixed $Cargo_id_cargo
     */
    public function setCargoIdCargo($Cargo_id_cargo)
    {
        $this->Cargo_id_cargo = $Cargo_id_cargo;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getSituacaoIdSituacao()
    {
        return $this->Situacao_id_situacao;
    }

    /**
     * @param mixed $Situacao_id_situacao
     */
    public function setSituacaoIdSituacao($Situacao_id_situacao)
    {
        $this->Situacao_id_situacao = $Situacao_id_situacao;
    }

    /**
     * @return mixed
     */
    public function getSetorIdSetor()
    {
        return $this->Setor_id_setor;
    }

    /**
     * @param mixed $Setor_id_setor
     */
    public function setSetorIdSetor($Setor_id_setor)
    {
        $this->Setor_id_setor = $Setor_id_setor;
    }



}