<?php
/**
 * Created by PhpStorm.
 * User: viniciushrk
 * Date: 05/10/2018
 * Time: 15:59
 */
require_once "Conexao.php";


class Transferencia extends Conexao
{
    private $id_transf;
    private $data_transf;
    private $hora_transf;
    private $observacao;
    private $Patrimonio_tombo;
    private $Setor_id_setor;

    public function selecionaPorIdTipoVinculo($id_transf)
    {
        try {
            $con = $this->conecta();
            $resul = $con->prepare("select * from Transferencia where id_transf= ?");
            $resul->bindValue(1, $id_transf);
            $resul->execute();
            $con = null;
            if ($resul->rowCount() > 0) {
                $resul = $resul->fetch();
                $this->id_transf = $id_transf;
                $this->data_transf= $resul[0];
                $this->hora_transf= $resul[1];
                $this->observacao= $resul[2];
                $this->Patrimonio_tombo= $resul[3];
                $this->Setor_id_setor= $resul[4];
            } else {
                return 0;
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getDataTransf()
    {
        return $this->data_transf;
    }

    /**
     * @param mixed $data_transf
     */
    public function setDataTransf($data_transf)
    {
        $this->data_transf = $data_transf;
    }

    /**
     * @return mixed
     */
    public function getHoraTransf()
    {
        return $this->hora_transf;
    }

    /**
     * @param mixed $hora_transf
     */
    public function setHoraTransf($hora_transf)
    {
        $this->hora_transf = $hora_transf;
    }

    /**
     * @return mixed
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param mixed $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getPatrimonioTombo()
    {
        return $this->Patrimonio_tombo;
    }

    /**
     * @param mixed $Patrimonio_tombo
     */
    public function setPatrimonioTombo($Patrimonio_tombo)
    {
        $this->Patrimonio_tombo = $Patrimonio_tombo;
    }

    /**
     * @return mixed
     */
    public function getSetorIdSetor()
    {
        return $this->Setor_id_setor;
    }

    /**
     * @param mixed $Setor_id_setor
     */
    public function setSetorIdSetor($Setor_id_setor)
    {
        $this->Setor_id_setor = $Setor_id_setor;
    }


}