<?php
/**
 * Created by PhpStorm.
 * User: viniciushrk
 * Date: 05/10/2018
 * Time: 15:31
 */

class Turno extends Conexao
{
    private $id_tipo;
    private $tipo;
    private $numero_serie;

    public function seleciona($id_tipo) //by PK
    {
        try {
            $con = $this->conecta();
            $resul = $con->prepare("select * from turno where id_tipo= ?");
            $resul->bindValue(1, $id_tipo);
            $resul->execute();
            $con = null;
            if ($resul->rowCount() > 0) {
                $resul = $resul->fetch();
                $this->id_tipo = $resul[0];
                $this->tipo = $resul[1];
                $this->numero_serie = $resul[2];
            } else {
                return 0;
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

}
